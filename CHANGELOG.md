# CHANGELOG

## Unreleased

Changes unreleased.

### Chore

- pre-commit:
  - Weekly pre-commit autoupdate (#52) ([1cec5ce](https://github.com/imAsparky/cookiecutter-py3-package/commit/1cec5ce9c4db54a2945690906ead6a3f4fd1f420)) ([#52](https://github.com/imAsparky/cookiecutter-py3-package/pull/52))

- git:
  - Add vscode workspace folder .gitignore ([c000227](https://github.com/imAsparky/cookiecutter-py3-package/commit/c00022746f83c123ec441bdd010fdd06e8f34b58))
  - Add project folders to .gitignore ([4847e57](https://github.com/imAsparky/cookiecutter-py3-package/commit/4847e574c6b978a3aab14313b12a74d1c879dcd6))

- setup:
  - Update to setuptools #27 ([c0d3b0d](https://github.com/imAsparky/cookiecutter-py3-package/commit/c0d3b0dc18a58a947a2d9eea5123a31ae39e843c)) ([#33](https://github.com/imAsparky/cookiecutter-py3-package/pull/33))

- Travis:
  - Remove support for Travis #18 ([ade1959](https://github.com/imAsparky/cookiecutter-py3-package/commit/ade1959381459abb179df2b3a7c1e25ae695f3db)) ([#23](https://github.com/imAsparky/cookiecutter-py3-package/pull/23))
  - Remove support #18 ([aa463f4](https://github.com/imAsparky/cookiecutter-py3-package/commit/aa463f412f26a03a9c1cc278b9bca56e54f114a2)) ([#21](https://github.com/imAsparky/cookiecutter-py3-package/pull/21))

- README:
  - Remove stale Similar Templates #11 ([2b62df5](https://github.com/imAsparky/cookiecutter-py3-package/commit/2b62df5f7cf28f6b10fcc17ecebc6dec6dd550e2))

- docs:
  - Comment on:push out for CHANGELOG ([ce7b075](https://github.com/imAsparky/cookiecutter-py3-package/commit/ce7b075187b060ca036e34a65f7f8e52a4ab1f0d))

- test:
  - Rename workflow #10 ([d4a709b](https://github.com/imAsparky/cookiecutter-py3-package/commit/d4a709bcc6f8a431363b1837c3e78a6718f0ec44))
  - Fix typo in jobs:name:  #10 ([38f6a89](https://github.com/imAsparky/cookiecutter-py3-package/commit/38f6a89d02b3512f143b6e74b775b8cdbaa2882a))
  - Add test coverage Py39 Py3-10 #7 ([a7a9219](https://github.com/imAsparky/cookiecutter-py3-package/commit/a7a921961c2d439ac0724fc539249520f73c6c72))

### Feature

- pre-commit:
  - Add to pre-commit to GH #31 ([bf25e6c](https://github.com/imAsparky/cookiecutter-py3-package/commit/bf25e6cf327670a6e7a38e1464de74a25e447205)) ([#32](https://github.com/imAsparky/cookiecutter-py3-package/pull/32))

- test:
  - Add GH action to test all contribs #10 ([9f0787d](https://github.com/imAsparky/cookiecutter-py3-package/commit/9f0787d7403429186948a38e4b33a1ea8c66ab71))

- docs:
  - Add automatic changelog #8 ([d14487a](https://github.com/imAsparky/cookiecutter-py3-package/commit/d14487aa3f3c190a1720884e1c875bdca055cb7d))
  - Add automatic changelog #8 ([0160b52](https://github.com/imAsparky/cookiecutter-py3-package/commit/0160b52a0a623cc48c8400a5700b1a7460cb6812))

- quality:
  - Add code quality scan and badge #5 ([fb8fe81](https://github.com/imAsparky/cookiecutter-py3-package/commit/fb8fe81f4d9439e78a33b00babfa2ab3a7ae380b))

- git:
  - Add pre-commit and sane tests #3 ([83a8804](https://github.com/imAsparky/cookiecutter-py3-package/commit/83a88044bfb0ce44e54eb05943f2eb2c4282d195))
  - Add conventional commits template #2 ([d808d8e](https://github.com/imAsparky/cookiecutter-py3-package/commit/d808d8ef08ca158f98bf5a302062f13ab503ab31))
  - Add additional issue templates #1 ([6f5c9da](https://github.com/imAsparky/cookiecutter-py3-package/commit/6f5c9da926b158d2116db2cee3f1bf3ac459c86b))

### Bug Fixes

- pre-com:
  - Fix pre-commit errors #50  (#51) ([9360c7f](https://github.com/imAsparky/cookiecutter-py3-package/commit/9360c7fe47a3e79ae421c5e43ce330414203b2b1)) ([#51](https://github.com/imAsparky/cookiecutter-py3-package/pull/51))

- CHANGELOG:
  - Fix tox.ini and conf.py #43 (#47) ([c7ead74](https://github.com/imAsparky/cookiecutter-py3-package/commit/c7ead747409c6b1997e2524fefd261f7259d0274)) ([#47](https://github.com/imAsparky/cookiecutter-py3-package/pull/47))

- git:
  - Comment out on:push:main  #44 (#45) ([3631126](https://github.com/imAsparky/cookiecutter-py3-package/commit/3631126832b3724859a2cfb58a834205c5948567)) ([#45](https://github.com/imAsparky/cookiecutter-py3-package/pull/45))
  - Remove test for py3.10 #26 ([e5fdef2](https://github.com/imAsparky/cookiecutter-py3-package/commit/e5fdef216b7adf4c0759d85fc2b90a733d2f4426))

- docs:
  - Add readthedocs.yaml config file #40 (#42) ([bc14391](https://github.com/imAsparky/cookiecutter-py3-package/commit/bc14391f508c585aa8a29f00150c75e4823bc4a0)) ([#42](https://github.com/imAsparky/cookiecutter-py3-package/pull/42))

### Documentation

- codes:
  - Add code of conduct #56 (#58) ([b69c126](https://github.com/imAsparky/cookiecutter-py3-package/commit/b69c1269178d77cee55983992b5a22737197d8b9)) ([#58](https://github.com/imAsparky/cookiecutter-py3-package/pull/58))

- README:
  - Add pre-commit badge #54 (#55) ([08450af](https://github.com/imAsparky/cookiecutter-py3-package/commit/08450af653d7b175a0327f51e5aaac7643f422c0)) ([#55](https://github.com/imAsparky/cookiecutter-py3-package/pull/55))
  - Add to list of notable changes (#24) ([5db0139](https://github.com/imAsparky/cookiecutter-py3-package/commit/5db0139e3c21190b3d2085d3c4947beeac2fea25)) ([#24](https://github.com/imAsparky/cookiecutter-py3-package/pull/24))
  - Remove support for Travis CI #18 ([0dd88fa](https://github.com/imAsparky/cookiecutter-py3-package/commit/0dd88fa56d5646caddd54828a2640fa96379d0af))
  - Update to reflect this fork #4 ([e592c57](https://github.com/imAsparky/cookiecutter-py3-package/commit/e592c5750e36aff5b8486e0324e8778510498d7c))

- tutorial:
  - Fix broken pypi checklist link #37 (#53) ([f6ec351](https://github.com/imAsparky/cookiecutter-py3-package/commit/f6ec3518368078b34d25293494b3332027ccb37b)) ([#53](https://github.com/imAsparky/cookiecutter-py3-package/pull/53))

- logo:
  - Create and add logo to README #38 (#41) ([0ca1adb](https://github.com/imAsparky/cookiecutter-py3-package/commit/0ca1adb6181138de7b6ccf06fa5984d2c693a169)) ([#41](https://github.com/imAsparky/cookiecutter-py3-package/pull/41))

- update:
  - Links->cookiecutter-py3-package #20 (#39) ([7522aa4](https://github.com/imAsparky/cookiecutter-py3-package/commit/7522aa4014eda677ec28bbd8154906f69afa8811)) ([#39](https://github.com/imAsparky/cookiecutter-py3-package/pull/39))

- structure:
  - Update to newer sphinx style #34 ([6f76c4e](https://github.com/imAsparky/cookiecutter-py3-package/commit/6f76c4ead0bb70be7f9630a0fb12ab8f6297bf7d)) ([#35](https://github.com/imAsparky/cookiecutter-py3-package/pull/35))

- fork:
  - Update to indicate this fork #20 ([f287b06](https://github.com/imAsparky/cookiecutter-py3-package/commit/f287b06c8bf40262728bfe2dfe5d542200d0b2ba))

\* *This CHANGELOG was automatically generated by [auto-generate-changelog](https://github.com/BobAnkh/auto-generate-changelog)*
