====================================================
Welcome to cookiecutter-py3-package's documentation!
====================================================

Getting Started
---------------

.. toctree::
   :maxdepth: 2

   readme
   tutorial
   pypi_release_checklist

Basics
------

.. toctree::
   :maxdepth: 2

   prompts

Advanced Features
-----------------

.. toctree::
   :maxdepth: 2

   console_script_setup


Changelog
---------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   CHANGELOG-docs.md




Todo List to improve cookiecutter-py3-package docs.
---------------------------------------------------

.. todolist::


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
